/**
 * This file is a part of Presenter.
 * A Presentation Software
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "Global.hpp"
#include "UI.hpp"

DesQ::Settings *presenterSett = nullptr;

int main( int argc, char **argv ) {
    QApplication app( argc, argv );

    /* About Application */
    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Presenter" );

    app.setQuitOnLastWindowClosed( true );

    presenterSett = new DesQ::Settings( "Presenter" );

    DesQ::Presenter::UI *Gui = new DesQ::Presenter::UI();

    Gui->showMaximized();

    qApp->processEvents();

    /** Try to load a PDF if specified in the CLI */
    if ( argc > 1 ) {
        Gui->load( argv[ 1 ] );
    }

    return app.exec();
}
