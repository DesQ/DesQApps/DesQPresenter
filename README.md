# DesQ Presenter
## A simple Presentation Software for the DesQ Desktop Environment

This is an app to enhance LaTeX Beamer's presentation experience.
With some small additions to Beamer, DesQ Presenter makes it possible to have animations (gif/mng), audio and video in the slides.

### Dependencies (Debian package name [version] in Sid):
* Qt5 (qtbase5-dev [5.15.2+dfsg-2], qtbase5-dev-tools [5.15.2+dfsg-2])
* libdesq (https://gitlab.com/DesQ/libdesq)
* libdesqui (https://gitlab.com/DesQ/libdesqui)
* qdocumentview (https://gitlab.com/marcusbritanicus/qdocumentview)


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQApps/DesQPresenter.git DesQPresenter`
- Enter the `DesQPresenter` folder
  * `cd DesQPresenter`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Upcoming
* Support for epub, djvu, cbr, cbz, etc.
* Support for poening documents in multiple tabs.
* Any other feature you request for... :)
