/**
 * This file is a part of Presenter.
 * A Presentation Software
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "UI.hpp"

DesQ::Presenter::UI::UI() : QWidget() {
    mPdfDoc = nullptr;

    createUI();

    setMouseTracking( true );
}


void DesQ::Presenter::UI::load( QString pdfFile ) {
    /**
     * Open the PDF Document
     * We will not perform any error checking, here or later.
     * It is assumed that the user is responsible for providing a proper pdf
     */
    mPdfDoc = new PopplerDocument( pdfFile );
    connect(
        mPdfDoc, &PopplerDocument::loading, [ = ]( int value ) {
            mPageW->setProgressValue( value );

            /** Once completely loaded, set the range to number of pages */
            if ( value == 100 ) {
                mPageW->setProgressRange( 0, mPdfDoc->pageCount() - 1 );
                mPageW->setProgressValue( 0 );
            }
        }
    );

    mPdfDoc->load();

    QDocumentPage *page = mPdfDoc->page( 0 );

    mPageSize = page->pageSize();
    mPageSize.scale( width(), height(), Qt::KeepAspectRatio );

    mSlideRect.setX( (width() - mPageSize.width() ) / 2.0 );
    mSlideRect.setY( (height() - mPageSize.height() ) / 2.0 );
    mSlideRect.setSize( mPageSize );

    /** Load the first and the second slide */
    slideMaker = new QDocumentRenderer( this );
    slideMaker->setDocument( mPdfDoc );

    connect(
        slideMaker, &QDocumentRenderer::pageRendered, [ = ]( int page ) {
            if ( page == mCurPage ) {
                repaint();
            }
        }
    );

    mCurPage = 0;

    slideMaker->requestPage( 0, mPageSize.toSize(), QDocumentRenderOptions() );
    slideMaker->requestPage( 1, mPageSize.toSize(), QDocumentRenderOptions() );

    connect( mPageW, &PageWidget::loadNext, this, &DesQ::Presenter::UI::handleNextClicked );
    connect( mPageW, &PageWidget::loadPrev, this, &DesQ::Presenter::UI::handlePrevClicked );

    updateButtons();
}


void DesQ::Presenter::UI::createUI() {
    /** Page and Progress Widget */
    mPageW = new PageWidget( this );

    /** Occupy the full screen area */
    setFixedSize( qApp->primaryScreen()->size() );

    /** Window Properties */
    setWindowTitle( "Presenter" );
    setWindowIcon( QIcon::fromTheme( "desq-presenter", QIcon( ":/desq-presenter.png" ) ) );

    setWindowFlags( Qt::Window | Qt::FramelessWindowHint );

    /** Linear Gradients */
    leftGrad = QLinearGradient( 0, 0, 0.1 * width(), 0 );
    leftGrad.setColorAt( 0.0, QColor( 255, 255, 255, 10 ) );
    leftGrad.setColorAt( 1.0, QColor( 255, 255, 255, 0 ) );

    rightGrad = QLinearGradient( 0.9 * width(), 0, width(), 0 );
    rightGrad.setColorAt( 0.0, QColor( 255, 255, 255, 0 ) );
    rightGrad.setColorAt( 1.0, QColor( 255, 255, 255, 10 ) );

    QAction *openAct = new QAction( QIcon::fromTheme( "document-open" ), "Open Presentation", this );
    openAct->setShortcut( tr( "Ctrl+O" ) );
    connect( openAct, &QAction::triggered, this, &DesQ::Presenter::UI::handleOpenRequested );
    addAction( openAct );
}


void DesQ::Presenter::UI::updateButtons() {
    /** Update the ProgressBar */
    mPageW->setProgressValue( mCurPage );

    /** Update Previous Button State */
    if ( mCurPage == 0 ) {
        mPageW->setPreviousEnabled( false );
    }

    else {
        mPageW->setPreviousEnabled( true );
    }

    /** Update Next Button State */
    if ( mCurPage >= mPdfDoc->pageCount() - 1 ) {
        mPageW->setNextEnabled( false );
    }

    else {
        mPageW->setNextEnabled( true );
    }
}


void DesQ::Presenter::UI::handlePrevClicked() {
    if ( mPdfDoc == nullptr ) {
        return;
    }

    if ( mCurPage > 0 ) {
        mCurPage--;
        repaint();

        updateButtons();

        /** Request a render of page before and after */
        slideMaker->requestPage( mCurPage + 1, mPageSize.toSize(), QDocumentRenderOptions() );
        slideMaker->requestPage( mCurPage - 1, mPageSize.toSize(), QDocumentRenderOptions() );
    }
}


void DesQ::Presenter::UI::handleNextClicked() {
    if ( mPdfDoc == nullptr ) {
        return;
    }

    if ( mCurPage < mPdfDoc->pageCount() - 1 ) {
        mCurPage++;
        repaint();

        updateButtons();

        /** Request a render of page before and after */
        slideMaker->requestPage( mCurPage + 1, mPageSize.toSize(), QDocumentRenderOptions() );
        slideMaker->requestPage( mCurPage - 1, mPageSize.toSize(), QDocumentRenderOptions() );
    }
}


void DesQ::Presenter::UI::handleOpenRequested() {
    QString filename = QFileDialog::getOpenFileName(
        this,
        "DesQ Presenter | Open Presentation",
        QDir::homePath(),
        "Presentation (*.pdf);;All files (*.*)"
    );

    if ( not filename.isEmpty() ) {
        load( filename );
    }
}


void DesQ::Presenter::UI::resizeEvent( QResizeEvent *rEvent ) {
    rEvent->accept();

    mPageW->move( rEvent->size().width() - 72 - 10, rEvent->size().height() - 40 - 10 );
}


void DesQ::Presenter::UI::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        if ( (mEvent->pos().x() <= width() * 0.1 ) or (mEvent->pos().x() >= width() * 0.9 ) ) {
            mMousePressed = true;
        }

        else {
            mMousePressed = false;
        }
    }

    QWidget::mousePressEvent( mEvent );
}


void DesQ::Presenter::UI::mouseMoveEvent( QMouseEvent *mEvent ) {
    if ( mEvent->pos().x() <= width() * 0.1 ) {
        mPaintLeft = true;
        mPaintRight = false;

        repaint();
    }

    else if ( mEvent->pos().x() >= width() * 0.9 ) {
        mPaintLeft = false;
        mPaintRight = true;

        repaint();
    }

    else {
        mPaintLeft = false;
        mPaintRight = false;

        repaint();
    }

    QWidget::mouseMoveEvent( mEvent );
}


void DesQ::Presenter::UI::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( mMousePressed ) {
        if ( mEvent->pos().x() <= width() * 0.1 ) {
            handlePrevClicked();
        }

        else {
            handleNextClicked();
        }
    }

    QWidget::mouseReleaseEvent( mEvent );
}


void DesQ::Presenter::UI::keyPressEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Right ) {
        handleNextClicked();
    }

    else if ( kEvent->key() == Qt::Key_Left ) {
        handlePrevClicked();
    }

    else if ( kEvent->key() == Qt::Key_Home ) {
        if ( mPdfDoc == nullptr ) {
            return;
        }

        mCurPage = 0;
        repaint();

        updateButtons();
    }

    else if ( kEvent->key() == Qt::Key_End ) {
        if ( mPdfDoc == nullptr ) {
            return;
        }

        mCurPage = mPdfDoc->pageCount() - 1;
        repaint();

        updateButtons();
    }

    else if ( kEvent->key() == Qt::Key_Escape ) {
        close();
    }

    QWidget::keyPressEvent( kEvent );
}


void DesQ::Presenter::UI::paintEvent( QPaintEvent *pEvent ) {
    /** No PDF */
    if ( mPdfDoc == nullptr ) {
        QPainter painter( this );
        painter.setRenderHints( QPainter::Antialiasing );

        QImage img( ":/desq-presenter.png" );
        painter.drawImage( QRectF( ( width() - 256.0 ) / 2.0, ( height() - 256.0 ) / 2.0, 256, 256 ), img );
        painter.drawText( QRectF( 0, height() / 2.0 + 256, width(), 24 ), Qt::AlignCenter, "Press 'Ctrl+O' to open a presentation." );
        painter.end();

        return;
    }

    /** Out of range, do nothing */
    if ( mCurPage < 1 and mCurPage >= mPdfDoc->pageCount() ) {
        return;
    }

    QPainter painter( this );
    painter.setRenderHints( QPainter::Antialiasing );

    painter.save();
    painter.fillRect( rect(),     Qt::black );
    painter.fillRect( mSlideRect, Qt::white );
    painter.drawImage( mSlideRect, slideMaker->requestPage( mCurPage, mPageSize.toSize(), QDocumentRenderOptions() ) );
    painter.restore();

    if ( mPaintLeft ) {
        painter.save();
        painter.setPen( Qt::NoPen );
        painter.setBrush( leftGrad );
        painter.drawRect( QRectF( 0, 0, 0.1 * width(), height() ) );
        painter.restore();
    }

    if ( mPaintRight ) {
        painter.save();
        painter.setPen( Qt::NoPen );
        painter.setBrush( rightGrad );
        painter.drawRect( QRectF( 0.9 * width(), 0, 0.1 * width(), height() ) );
        painter.restore();
    }

    painter.end();

    QWidget::paintEvent( pEvent );
}
