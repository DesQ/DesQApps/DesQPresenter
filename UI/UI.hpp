/**
 * This file is a part of Presenter.
 * A Presentation Software
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include "Global.hpp"
#include "Widgets.hpp"
#include <qdocumentview/QDocument.hpp>
#include <qdocumentview/PopplerDocument.hpp>
#include <qdocumentview/QDocumentRenderer.hpp>

namespace DesQ {
    namespace Presenter {
        class UI;
    }
}


class DesQ::Presenter::UI : public QWidget {
    Q_OBJECT;

    public:
        UI();

        void load( QString );

    private:
        PopplerDocument *mPdfDoc;

        PageWidget *mPageW;
        int mCurPage = 0;

        QSizeF mPageSize;
        QRectF mSlideRect;

        QDocumentRenderer *slideMaker;

        bool mMousePressed = false;

        QLinearGradient leftGrad;
        QLinearGradient rightGrad;

        bool mPaintLeft = false;
        bool mPaintRight = false;

        void createUI();
        void updateButtons();

        void handlePrevClicked();
        void handleNextClicked();

        void handleOpenRequested();

    protected:
        void resizeEvent( QResizeEvent *rEvent ) override;
        void paintEvent( QPaintEvent *pEvent ) override;

        void mousePressEvent( QMouseEvent * ) override;
        void mouseMoveEvent( QMouseEvent * ) override;
        void mouseReleaseEvent( QMouseEvent * ) override;

        void keyPressEvent( QKeyEvent * ) override;
};
