/**
 * This file is a part of Presenter.
 * A Presentation Software
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#include "Widgets.hpp"

PageWidget::PageWidget( QWidget *parent ) : QWidget( parent ) {
    setFixedSize( QSize( 72, 34 ) );

    btnState = { false, false, false, false, false };

    previousRect     = QRectF( 5, 2, 28, 24 );
    nextRect         = QRectF( 38, 2, 28, 24 );
    progressBaseRect = QRectF( 2, 28, 68, 4 );
    progressRect     = QRectF( 3, 29, 0, 2 );

    isPreviousEnabled = false;
    isNextEnabled     = false;

    setMouseTracking( true );

    previousIcon = QIcon::fromTheme( "arrow-left", QIcon( ":/prev.png" ) ).pixmap( QSize( 24, 24 ), QIcon::Normal );
    nextIcon     = QIcon::fromTheme( "arrow-right", QIcon( ":/next.png" ) ).pixmap( QSize( 24, 24 ), QIcon::Normal );
}


void PageWidget::setPreviousEnabled( bool yes ) {
    isPreviousEnabled = yes;
    repaint();
}


void PageWidget::setNextEnabled( bool yes ) {
    isNextEnabled = yes;
    repaint();
}


void PageWidget::setProgressRange( int minVal, int maxVal ) {
    mProgressMin = minVal;
    mProgressMax = maxVal;

    mProgress = 0;

    update();
}


void PageWidget::setProgressValue( int value ) {
    if ( value < mProgressMin ) {
        value = mProgressMin;
    }

    if ( value > mProgressMax ) {
        value = mProgressMax;
    }

    if ( mProgress == value ) {
        return;
    }

    mProgress = value;
    // qDebug() << (progressBaseRect.width() - 2) << (mProgress - mProgressMin) << (mProgressMax - mProgressMin);
    progressRect.setWidth( (progressBaseRect.width() - 2) * (mProgress - mProgressMin) / (mProgressMax - mProgressMin) );

    repaint();
}


void PageWidget::enterEvent( QEvent *event ) {
    btnState.mouseIn = true;

    repaint();
    event->accept();
}


void PageWidget::leaveEvent( QEvent *event ) {
    btnState.mouseIn = false;

    repaint();
    event->accept();
}


void PageWidget::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        // Left half = previous button pressed
        if ( previousRect.contains( mEvent->localPos() ) ) {
            btnState = { true, isPreviousEnabled, false, isPreviousEnabled, false };
        }

        else if ( nextRect.contains( mEvent->localPos() ) ) {
            btnState = { true, false, isNextEnabled, false, isNextEnabled };
        }

        else {
            btnState = { true, false, false, false, false };
        }
    }

    repaint();
    mEvent->accept();
}


void PageWidget::mouseMoveEvent( QMouseEvent *mEvent ) {
    if ( btnState.previousPressed or btnState.nextPressed ) {
        repaint();
        QWidget::mouseMoveEvent( mEvent );

        return;
    }

    if ( previousRect.contains( mEvent->localPos() ) ) {
        btnState = { true, isPreviousEnabled, false, false, false };
    }

    else if ( nextRect.contains( mEvent->localPos() ) ) {
        btnState = { true, false, isNextEnabled, false, false };
    }

    else {
        btnState = { true, false, false, false, false };
    }

    repaint();
    QWidget::mouseMoveEvent( mEvent );
}


void PageWidget::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        // previous-button was pressed and release
        if ( previousRect.contains( mEvent->localPos() ) ) {
            if ( btnState.previousPressed and isPreviousEnabled ) {
                emit loadPrev();
            }
        }

        else if ( nextRect.contains( mEvent->localPos() ) ) {
            if ( btnState.nextPressed and isNextEnabled ) {
                emit loadNext();
            }
        }
    }

    btnState = { true, false, false, false, false };

    repaint();
    mEvent->accept();
}


void PageWidget::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );

    painter.fillRect( rect(), Qt::transparent );

    QColor borderClr = palette().color( QPalette::Shadow );
    QColor bgClr     = palette().color( QPalette::Window );

    if ( not btnState.mouseIn ) {
        // Draw at 10% opacity
        painter.setOpacity( 0.1 );
    }

    /* Background + Border */
    painter.save();
    painter.setPen( Qt::NoPen );
    painter.setBrush( bgClr );
    painter.drawRoundedRect( QRectF( rect() ), 3, 3 );
    painter.restore();

    /* Separator */
    painter.save();
    painter.setRenderHint( QPainter::Antialiasing, false );
    painter.setPen( borderClr );
    painter.drawLine( QPoint( 35, 5 ), QPoint( 35, 22 ) );
    painter.restore();

    if ( btnState.mouseIn ) {
        /* Highlight */
        painter.save();
        painter.setOpacity( 0.25 );

        if ( btnState.previousPressed or btnState.nextPressed ) {
            painter.setOpacity( 0.35 );
        }

        if ( btnState.inPrevious ) {
            painter.setPen( Qt::transparent );
            painter.setBrush( palette().color( QPalette::Highlight ) );
            painter.drawRoundedRect( previousRect, 3, 3 );
        }

        if ( btnState.inNext ) {
            painter.setPen( Qt::transparent );
            painter.setBrush( palette().color( QPalette::Highlight ) );
            painter.drawRoundedRect( nextRect, 3, 3 );
        }

        painter.restore();
    }

    /* Previous and Next icons */
    painter.save();
    painter.drawPixmap( previousRect.topLeft() + QPointF( 2.0, 0 ), previousIcon );
    painter.drawPixmap( nextRect.topLeft() + QPointF( 2.0, 0 ),     nextIcon );
    painter.restore();

    /* ProgressBar */
    painter.save();
    painter.setPen( Qt::NoPen );
    painter.setBrush( borderClr );
    painter.drawRoundedRect( progressBaseRect, 2.0, 2.0 );
    if ( mProgress ) {
        painter.setBrush( palette().color( QPalette::Highlight ) );
        painter.drawRoundedRect( progressRect, 1.0, 1.0 );
    }
    painter.restore();

    painter.end();

    QWidget::paintEvent( pEvent );
}
