/**
 * This file is a part of Presenter.
 * A Presentation Software
 * Copyright 2018-2022 Marcus Britanicus
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, vsit http://www.gnu.org/licenses/.
 **/

#pragma once

#include "Global.hpp"

class PageWidget : public QWidget {
    Q_OBJECT

    public:
        PageWidget( QWidget *parent );

        void setPreviousEnabled( bool );
        void setNextEnabled( bool );

        void setProgressRange( int, int );
        void setProgressValue( int );

    private:
        typedef struct _state {
            bool mouseIn;
            bool inPrevious;
            bool inNext;
            bool previousPressed;
            bool nextPressed;
        } ZoomBtnState;

        bool isPreviousEnabled = false;
        bool isNextEnabled     = false;

        ZoomBtnState btnState;

        QRectF nextRect;
        QRectF previousRect;

        QRectF progressRect;
        QRectF progressBaseRect;

        QPixmap nextIcon;
        QPixmap previousIcon;

        int mProgressMin = 0;
        int mProgressMax = 0;
        int mProgress    = 0;

    protected:
        void enterEvent( QEvent * ) override;
        void leaveEvent( QEvent * ) override;

        void mousePressEvent( QMouseEvent * ) override;
        void mouseMoveEvent( QMouseEvent * ) override;
        void mouseReleaseEvent( QMouseEvent * ) override;

        void paintEvent( QPaintEvent *pEvent ) override;

    Q_SIGNALS:
        void loadNext();
        void loadPrev();
};
