project(
    'DesQ Presenter',
    'c',
		'cpp',
		version: '1.0.0',
		license: 'GPLv3',
		meson_version: '>=0.59.0',
		default_options: [
				'cpp_std=c++17',
				'c_std=c11',
				'warning_level=2',
				'werror=false',
		],
)

add_global_arguments( '-DPROJECT_VERSION="v@0@"'.format( meson.project_version() ), language : 'cpp' )
add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

GlobalInc = include_directories( '.' )

# Qt5 Module, for moc, and uic
Qt5 = import( 'qt5' )

# Qt5 dependencies
Qt5Core    = dependency( 'qt5', modules: [ 'Core' ], private_headers: [ 'Core' ] )
Qt5Gui     = dependency( 'qt5', modules: [ 'Gui' ], private_headers: [ 'Gui' ] )
Qt5Widgets = dependency( 'qt5', modules: [ 'Widgets' ], private_headers: [ 'Widgets' ] )

# DesQ dependencies
DesQGui     = dependency( 'desq-gui' )
DesQWidgets = dependency( 'desq-widgets' )

# QDocumentView
PopplerQt5 = dependency( 'poppler-qt5' )
DjVuLibre  = dependency( 'ddjvuapi' )
QDV        = dependency( 'Qt5DocumentView' )

Qt5Deps = [ Qt5Core, Qt5Gui, Qt5Widgets ]
Deps = [ Qt5Deps, DesQGui, DesQWidgets, PopplerQt5, DjVuLibre, QDV ]

Headers = [
    'UI/UI.hpp',
    'UI/Widgets.hpp'
]

Sources = [
		'Main.cpp',
    'UI/UI.cpp',
    'UI/Widgets.cpp'
]

Mocs = Qt5.compile_moc(
 		headers : Headers,
	 	dependencies: Qt5Deps
)

Resources = Qt5.compile_resources(
		name: 'presenter_rcc',
		sources: 'icons/icons.qrc'
)

presenter = executable(
    'desq-presenter', [ Sources, Mocs, Resources ],
    dependencies: Deps,
		include_directories: [ 'UI' ],
    install: true
)

install_data(
		'desq-presenter.desktop',
		install_dir: join_paths( get_option( 'prefix' ), get_option( 'datadir' ), 'applications' )
)

# install_data(
		# 'DesQPresenter.conf',
		# install_dir: join_paths( get_option( 'prefix' ), get_option( 'datadir' ), 'desq', 'configs' )
# )

install_data(
    'README.md', 'Changelog', 'ReleaseNotes',
    install_dir: join_paths( get_option( 'datadir' ), 'desq-docs' ),
)

install_data(
    'icons/desq-presenter.svg',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', 'scalable', 'apps' ),
)

install_data(
    'icons/desq-presenter.png',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', '256x256', 'apps' ),
)

summary = [
	'',
	'---------------',
	'DesQ Docs @0@'.format( meson.project_version() ),
	'---------------',
	''
]
message( '\n'.join( summary ) )
